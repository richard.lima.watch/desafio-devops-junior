# Teste Prático - DevOps Junior

## Plataforma de Streaming

### Objetivo
Simular a configuração e gerenciamento da infraestrutura de uma plataforma de streaming de vídeos, séries e canais ao vivo utilizando ferramentas locais e gratuitas. Implementar pipelines de CI/CD para automatizar o deploy de uma aplicação de streaming.

### Instruções

#### 1. Configuração da Infraestrutura Local

##### Docker

- Configure um ambiente Docker no seu computador.
- Crie um contêiner Docker que simule um servidor de streaming (por exemplo, usando uma aplicação Node.js ou uma solução pré-configurada como Nginx com RTMP module).

##### Docker Compose

- Use Docker Compose para orquestrar múltiplos contêineres, incluindo o servidor de streaming e um banco de dados (por exemplo, PostgreSQL ou MySQL).

##### MinIO (alternativa ao S3)

- Configure um servidor MinIO localmente para simular o armazenamento de objetos como vídeos. MinIO é uma alternativa gratuita ao Amazon S3 que pode ser executada localmente.

#### 2. Implementação de CI/CD

##### GitLab CI/CD

- Crie um repositório no GitLab e configure um pipeline de CI/CD usando o GitLab CI/CD (GitLab oferece runners gratuitos para CI/CD).
- O pipeline deve incluir etapas para:
  - Build da imagem Docker.
  - Teste da aplicação (você pode criar testes básicos para a aplicação de streaming).
  - Deploy da aplicação localmente usando Docker Compose.

#### 3. Monitoramento e Logging

##### Prometheus e Grafana

- Configure Prometheus e Grafana localmente para monitorar o desempenho da aplicação de streaming. Você pode utilizar exporters do Prometheus para coletar métricas do Docker.

##### Elastic Stack (ELK)

- Configure o Elastic Stack (Elasticsearch, Logstash, Kibana) localmente para coletar e visualizar logs da aplicação de streaming.

#### 4. Segurança e Gestão de Acessos

##### Traefik (opcional)

- Configure o Traefik como um proxy reverso local para gerenciar o tráfego da aplicação de streaming. Utilize autenticação básica para proteger o acesso.

#### 5. Documentação e Entrega

##### Documentação

- Prepare uma documentação detalhada explicando cada passo realizado, incluindo a configuração da infraestrutura local, implementação do CI/CD, monitoramento e logging, e gestão de acessos.

##### Entrega

- Submeta os arquivos Docker Compose, o Dockerfile da aplicação, o arquivo .gitlab-ci.yml do GitLab CI/CD, e a documentação detalhada.

### Critérios de Avaliação

- Correção e completude da infraestrutura provisionada localmente.
- Eficácia e eficiência do pipeline CI/CD.
- Adequação das configurações de monitoramento e logging.
- Clareza e detalhamento da documentação fornecida.
- Adesão às boas práticas de segurança e gestão de acessos.

### Duração

O teste prático deve ser realizado em até 48 horas.

### Instruções Adicionais

- Docker e Docker Compose podem ser instalados localmente e são gratuitos.
- MinIO pode ser instalado localmente seguindo a documentação oficial.
- Prometheus e Grafana podem ser instalados seguindo a documentação oficial do Prometheus e documentação oficial do Grafana.
- Elastic Stack (ELK) pode ser instalado localmente seguindo a documentação oficial.
- Traefik é opcional, mas pode ser instalado seguindo a documentação oficial.

### Como Contribuir

- Faça um fork deste repositório.
- Crie uma branch com a sua feature: git checkout -b feature/nova-feature
- Faça commit das suas mudanças: git commit -m 'Adiciona nova feature'
- Faça push para a sua branch: git push origin feature/nova-feature
- Faça um pull request neste repositório.

# Boa Sorte!!
